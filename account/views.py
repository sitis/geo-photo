from django.contrib.auth import authenticate, login, logout
from django.core.validators import email_re
from django.contrib.auth.models import User
from django.http import HttpResponse
from django.shortcuts import redirect
from account.models import sessions
from django.core.mail import send_mail
from django.conf import settings

import urllib2
import json
import string
import random
import hashlib


def login_user(request, user):
	
    """
    Log in user without requiring credentials (using ``login`` from
    ``django.contrib.auth``, first finding a matching backend).
	
    """
	
    from django.contrib.auth import load_backend, login
    if not hasattr(user, 'backend'):
        for backend in settings.AUTHENTICATION_BACKENDS:
            if user == load_backend(backend).get_user(user.pk):
                user.backend = backend
                break
    if hasattr(user, 'backend'):
        return login(request, user)


def create_account(name, email='', pwd=''):
	
	""" Create user account """
	
	try:
		user = User.objects.create_user(name, email, pwd)
	except:
		return False
	else:
		out = {}
		namehash = hashlib.md5(name).hexdigest()
		ss = sessions(session_user_id=user.id,session_data=out,session_hash=namehash)
		ss.save()
		return user


def activate(request, user_id):
	
	""" Activate account """
	
	if request.method == 'GET':
		if request.GET.has_key('key'):
			try:
				get = User.objects.get(id = user_id)
			except:
				return HttpResponse('error')
			else:
				load_session(request, user_id)
				if request.session['data'].has_key('email_activation'):
					if request.session['data']['email_activation'] == request.GET['key']:
						del request.session['data']['email_activation']
						save_session(request, user_id)
						return HttpResponse('<div style="font-size: 30px;color: #333;text-align: center;padding-top: 20px">Account activated. Proceed to sign in. <a href="http://geo-photo.net">http://geo-photo.net</a></div>')
				else:
					return HttpResponse('<div style="font-size: 30px;color: #333;text-align: center;padding-top: 20px">Account is already activated. Proceed to sign in. <a href="http://geo-photo.net">http://geo-photo.net</a></div>')


def register(request):
	
	""" User register action """
	
	if request.method == 'POST':
		
		out = {}
		
		# check login
		if request.POST.has_key('lgn'):
			name = urllib2.unquote(request.POST['lgn'])
			if len(name) < 4 or len(name) > 20:
				return HttpResponse('login <4 or >20')
			else:
				try:
					user = User.objects.get(username=name)
				except:
					pass
				else:
					out['double_login'] = 1
		
		# check email
		if request.POST.has_key('email'):
			mail = request.POST['email']
			if email_re.match(mail):
				try:
					get = User.objects.get(email = mail)
				except:
					pass
				else:
					out['double_email'] = 1
			else:
				out['invalid_email'] = 1
		
		if len(out) == 0:
			
			key = id_generator(25)
			
			# create user
			_user = User.objects.create_user(name, mail, request.POST['pwd'])
			_user.save()
			
			# 'not_activated' -> true
			# account needs to be activated through email
			_out = {}
			_out['email_activation'] = key
			namehash = hashlib.md5(name).hexdigest()
			ss = sessions(session_user_id=_user.id,session_data=json.dumps(_out),session_hash=namehash)
			ss.save()
			
			# generate mail
			try:
				send_mail('geo-photo.net email activation', 'Follow the link or copy the string to browser: http://deploy.geo-photo.net/account/activate/' + str(_user.id) + '/?key=' + str(key), 'no-reply@geo-photo.net', [mail], fail_silently=False)
			except:
				return HttpResponse('1')
			else:
				pass
			
			return HttpResponse(0)
		else:
			return HttpResponse(json.dumps(out))


def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
	
	""" Generate random string """
	
	return ''.join(random.choice(chars) for x in range(size))


def lgn(request):
	
	""" LOGIN processing """
	
	if request.method == 'POST':
		lgn   = urllib2.unquote(request.POST["lgn"])
		pwd   = urllib2.unquote(request.POST["pwd"])
		user  = authenticate(username=lgn, password=pwd)
		if user is not None:
			login(request, user)
			load_session(request)
			if request.session['data'].has_key('email_activation'):
				logout(request)
				return HttpResponse(2)
			else:
				return HttpResponse(1)
		else:
			return HttpResponse(0)


def log_out(request):
	
	""" LOGOUT """
	
	save_session(request)
	logout(request)
	return redirect('front_end.views.index')


def load_session(request, user_id=False):
	
	""" Load session """
	
	if user_id !=False:
		_user_id = user_id
	else:
		_user_id = request.user.id
	try:
		ss = sessions.objects.get(session_user_id=_user_id)
	except:
		return False
	else:
		request.session['data'] = {}
		request.session['data'] = json.loads(ss.session_data)


def save_session(request, user_id=False):
	
	""" Save session """
	
	try:
		out = json.dumps(request.session['data'])
	except:
		return False
	else:
		
		if user_id != False:
			_user_id = user_id
			namehash = ''
		else:
			_user_id = request.user.id
			namehash = hashlib.md5(request.user.username.encode('utf8')).hexdigest()
		
		try:
			ss = sessions.objects.get(session_user_id=_user_id)
		except:
			ss = sessions(session_user_id=_user_id,session_data=out,session_hash=namehash)
			ss.save()
		else:
			ss.session_data = out
			ss.save()
		return


