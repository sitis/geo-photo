from django.http import HttpResponse
from projects.models import shared
from django.shortcuts import render_to_response
from django.template import RequestContext
from front_end.models import projects
from django.shortcuts import redirect


from account.views import load_session
from account.views import save_session


def project_router(request, user_id, prj_id):
	
	""" Check for project url """
	
	if request.user.is_authenticated():
		
		return redirect('/id' + str(request.user.id))
		
		load_session(request)
		
		if int(request.user.id) == int(user_id):
			# user home page id
			# check if the project ID matches
			#_id = get_project_id_by_name(user_id, request.session['data']['working_project'])
			#return HttpResponse(request.session['data']['working_project'])
			_id = request.session['data']['working_project']
			if int(prj_id) != int(_id):
				#return redirect('/id' + str(user_id) + '/' + str(_id) + '/')
				pass
			else:
				user_id=''
				prj_id=''
			##else:
				##return render_to_response('dashboard2.html', {'user_name': request.user.username, 'user_id': user_id}, context_instance=RequestContext(request))
			return redirect()
		else:
			# not a user home page, request
			"""
			if is_project_shared(prj_id, user_id) != False:
				user_id = ''
				prj_id = ''
			else:
				return HttpResponse('project is not shared or not exist')
			"""
			
			
		return render_to_response('dashboard2.html', {'user_name': request.user.username, 'user_id': user_id, 'shared_project': prj_id, 'shared_user_id': user_id}, context_instance=RequestContext(request))
	
	# user is not authenticated
	
	else:
		if is_project_shared(prj_id, user_id) != False:
			# render project
			return render_to_response('dashboard2.html', {'user_name': 'SHARED', 'user_id': user_id, 'shared_project': prj_id, 'shared_user_id': user_id}, context_instance=RequestContext(request))
		else:
			return HttpResponse('project not exist or it is hidden')
		
		#return HttpResponse('<b>user is not authenticated</b> but we need to check if the project is shared and make a design to render all the widgets')
		# check for shared project


def get_project_id_by_name(user_id, prj_name):
	
	""" Get project id by name """
	
	try:
		a = projects.objects.get(project_name=prj_name, project_user_id=user_id)
	except:
		return False
	else:
		return a.project_id


def is_project_shared(prj_id, user_id):
	
	""" Check if the project is shared (by id) """
	
	try:
		a = shared.objects.get(shared_user_id=user_id, shared_project_id=prj_id)
	except:
		return False
	else:
		return True
