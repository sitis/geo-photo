
"""
This is only GOOGLE oauth library
TODO: facebook and twitter must be removed
"""

from django.http import HttpResponse
from django.shortcuts import redirect
from django.contrib.auth.models import User

from account.models import sessions

from account.views import create_account
from account.views import login_user
from mobile.views import isApple
from mobile.views import isAndroid

import time
import httplib
import hmac
import binascii
import hashlib
import urllib
import urllib2
import httplib2
import json

import oauth as oauth


KEY = 'FpFoKP_u7AOt7z_osf9VfdoO'
CLIENT_ID = '280977112637-n9l3v3bnsv4f8g1e92gifnbeif7t9tgo.apps.googleusercontent.com'

AUTH_URI = 'https://accounts.google.com/o/oauth2/auth'
TOKEN_URI = 'https://accounts.google.com/o/oauth2/token'
REDIRECT = 'http://deploy.geo-photo.net/oauth2callback'
SCOPE = 'https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email'


def google(request):
	
	""" Google oauth handler """
	
	url = str(AUTH_URI + '?redirect_uri=' + urllib.quote_plus(REDIRECT) + '&response_type=code&client_id=' + urllib.quote_plus(CLIENT_ID) + '&approval_prompt=force&scope=' + urllib.quote_plus(SCOPE) + '&access_type=offline')
	
	return redirect(url)


def auth_return(request):
	
	""" Return auth credentials """
	
	if request.GET.has_key('code'):
		code = request.GET['code']
		params = {
			'code': code,
			'client_id': CLIENT_ID,
			'client_secret': KEY,
			'redirect_uri': REDIRECT,
			'grant_type': 'authorization_code'
		}
		
		payload = urllib.urlencode(params)
		req = urllib2.Request(TOKEN_URI, payload)
		data = json.loads(urllib2.urlopen(req).read())
		req1 = urllib2.Request('https://www.googleapis.com/oauth2/v1/userinfo?access_token=' + data['access_token'])
		userdata = json.loads(urllib2.urlopen(req1).read())
		
		"""
		Sample response at endpoint
		
		{
			"id": "112905124788730153053",
			"email": "dmitry.vasechkin@gmail.com",
			"verified_email": true,
			"name": "Dmitry Vasechkin",
			"given_name": "Dmitry",
			"family_name": "Vasechkin",
			"link": "https://plus.google.com/112905124788730153053",
			"picture": "https://lh4.googleusercontent.com/-maExVD3PzEc/AAAAAAAAAAI/AAAAAAAAABg/7EJ4nwJJodU/photo.jpg",
			"gender": "male",
			"birthday": "0000-04-04",
			"locale": "ru"
		}
		"""
		
		userdata['pwd'] = ''
		
		try:
			user = User.objects.get(username=userdata['name'], email=userdata['email'])
		except:
			user = create_account(userdata['name'].encode('utf-8'), userdata['email'].encode('utf-8'))
			if user == False:
				return HttpResponse('Cannot create user. Contact site administrator.')
			namehash = hashlib.md5(user.username.encode('utf8')).hexdigest()
		else:
			namehash = hashlib.md5(user.username.encode('utf8')).hexdigest()
			try:
				ss = sessions.objects.get(session_user_id=user.id)
			except:
				out = {}
				#namehash = hashlib.md5(user.username.encode('utf8')).hexdigest()
				ss = sessions(session_user_id=user.id,session_data=out,session_hash=namehash)
				ss.save()
			else:
				#namehash = hashlib.md5(user.username.encode('utf8')).hexdigest()
				pass
		
		a = HttpResponse('<script type="text/javascript">window.opener.location.reload(false);window.close()</script>')
		
		#a = HttpResponse(namehash)
		
		if isApple(request) != False:
			request = urllib2.Request('geocam://_trace=' + str(namehash) + ';name= ' + str(userdata['name']) + ';status=200')
			return a
		if isAndroid(request) != False:
			a.set_cookie('_trace', namehash)
			a.set_cookie('status', '200')
			return a
		
		login_user(request, user)
		return a
		
	else:
		return HttpResponse('Auth cancelled.')


def facebook(request):
	
	""" Debug facebook auth TODO: remove from here"""
	
	pass


def get_normalized_http_method(self):
	
	"""Uppercases the http method."""
	
	return self.request.http_method.upper()
	

def escape(s):
	
    """Escape a URL including any /."""
    
    return urllib.quote(s, safe='~')
    

def build_signature_base_string(oauth_request, consumer, token):
	
	sig = (
		escape('POST'),
		escape(TW_TOKEN_URL),
		oauth_request
	)
	
	key = '%s&' % escape(consumer)
	if token:
		key += escape(token)
	raw = '&'.join(sig)
	return key, raw


def build_signature(oauth_request, consumer, token):
	
	"""Builds the base signature string."""
	
	key, raw = build_signature_base_string(oauth_request, consumer, token)
	
	# HMAC object.
	try:
		import hashlib # 2.5
		hashed = hmac.new(key, raw, hashlib.sha1)
	except:
		import sha # Deprecated
		hashed = hmac.new(key, raw, sha)
	
	return binascii.b2a_base64(hashed.digest())[:-1]


TW_CONSUMER_KEY = '189E1P4NrEuzxXpFKYA'
TW_CONSUMER_SECRET = 'EQScD3Dq4oKzHvkJwmmFXY4UJgnsusplyUnRAxWAU'
TW_TOKEN_URL = 'https://api.twitter.com/oauth/request_token'
TW_AUTH_URL = 'https://api.twitter.com/oauth/authorize'
TW_ACCESS_URL = 'https://api.twitter.com/oauth/access_token'
TW_ACCESS_TOKEN = '274868795-JXGDtLfrlICU885tZ8zoK9ez78XWFPxS8ihNEcNR'
TW_ACCESS_TOKEN_SECRET = 'a33LoqqwfMbgbuKnkWAXtv9Upc2DvsyLiaGGENA80'
TW_CALLBACK_URL = 'http://deploy.geo-photo.net/oauth/twitter/callback/'


""" TWITTER """
def twitter(request):
		
	
	REQUEST_TOKEN_URL = 'https://api.twitter.com/oauth/request_token'
	ACCESS_TOKEN_URL = 'https://api.twitter.com/oauth/access_token'
	AUTHORIZATION_URL = 'https://api.twitter.com/oauth/authorize'
	CALLBACK_URL = 'http://deploy.geo-photo.net/oauth/twitter/callback/'
	RESOURCE_URL = 'http://photos.example.net/photos'
	
	# key and secret granted by the service provider for this consumer application - same as the MockOAuthDataStore
	CONSUMER_KEY = '189E1P4NrEuzxXpFKYA'
	CONSUMER_SECRET = 'EQScD3Dq4oKzHvkJwmmFXY4UJgnsusplyUnRAxWAU'
	
	client = SimpleOAuthClient('127.0.0.1', 50501, REQUEST_TOKEN_URL, ACCESS_TOKEN_URL, AUTHORIZATION_URL)
	consumer = oauth.OAuthConsumer(CONSUMER_KEY, CONSUMER_SECRET)
	signature_method_plaintext = oauth.OAuthSignatureMethod_PLAINTEXT()
	signature_method_hmac_sha1 = oauth.OAuthSignatureMethod_HMAC_SHA1()
	
	oauth_request = oauth.OAuthRequest.from_consumer_and_token(consumer, callback=CALLBACK_URL, http_url=client.request_token_url)
	a = oauth_request.sign_request(signature_method_hmac_sha1, consumer, None)
	return HttpResponse(a)
	token = client.fetch_request_token(oauth_request)
	
	return HttpResponse(token)
	"""
	
	timestamp = int(time.time())
	
	params = {
		'oauth_callback': TW_CALLBACK_URL,
		'oauth_consumer_key': TW_CONSUMER_KEY,
		#'oauth_nonce': timestamp,
		'oauth_signature_method': 'HMAC-SHA1',
		'oauth_timestamp': timestamp,
		#'oauth_token': TW_ACCESS_TOKEN,
		'oauth_version': '1.0'
	}
	
	payload = 'POST&' + urllib.quote(TW_TOKEN_URL) + '&' + urllib.urlencode(params)
	
	sig = build_signature(payload, TW_CONSUMER_SECRET, TW_ACCESS_TOKEN_SECRET)
	#return HttpResponse(sig)
	
	params['oauth_signature'] = sig
	
	signed = urllib.urlencode(params)
	return HttpResponse(signed)
	req = urllib2.Request(TW_TOKEN_URL, payload)
	data = urllib2.urlopen(req).read()
	
	return HttpResponse(data)
	"""
	
	"""
	url = str(AUTH_URI + '?redirect_uri=' + urllib.quote_plus(REDIRECT) + '&response_type=code&client_id=' + urllib.quote_plus(CLIENT_ID) + '&approval_prompt=force&scope=' + urllib.quote_plus(SCOPE) + '&access_type=offline')
	
	return redirect(url)
	"""


def twitter_callback(request):
	return HttpResponse('twitter ok')


class SimpleOAuthClient(oauth.OAuthClient):

    def __init__(self, server, port=httplib.HTTP_PORT, request_token_url='', access_token_url='', authorization_url=''):
        self.server = server
        self.port = port
        self.request_token_url = request_token_url
        self.access_token_url = access_token_url
        self.authorization_url = authorization_url
        self.connection = httplib.HTTPConnection("%s:%d" % (self.server, self.port))

    def fetch_request_token(self, oauth_request):
        # via headers
        # -> OAuthToken
        self.connection.request(oauth_request.http_method, self.request_token_url, headers=oauth_request.to_header()) 
        response = self.connection.getresponse()
        return oauth.OAuthToken.from_string(response.read())

    def fetch_access_token(self, oauth_request):
        # via headers
        # -> OAuthToken
        self.connection.request(oauth_request.http_method, self.access_token_url, headers=oauth_request.to_header()) 
        response = self.connection.getresponse()
        return oauth.OAuthToken.from_string(response.read())

    def authorize_token(self, oauth_request):
        # via url
        # -> typically just some okay response
        self.connection.request(oauth_request.http_method, oauth_request.to_url()) 
        response = self.connection.getresponse()
        return response.read()

    def access_resource(self, oauth_request):
        # via post body
        # -> some protected resources
        headers = {'Content-Type' :'application/x-www-form-urlencoded'}
        self.connection.request('POST', RESOURCE_URL, body=oauth_request.to_postdata(), headers=headers)
        response = self.connection.getresponse()
        return response.read()

